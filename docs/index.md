# Présentations avec codimd

Diaporamas utilisant le type slide de codiMD

## Outils nationaux

Ensemble des outils [Apps Education](https://codimd.apps.education.fr/p/ao-fb52V7#/)

Ensemble des applications [Capytale](https://codimd.apps.education.fr/p/AmhLFkIxQ#/)

Présentation rapide de la [Forge des Communs Numériques Éducatifs](https://codimd.apps.education.fr/p/o2StBCJHw#/)

Exemple d'intégration :

`<embed width="760" height="448" src="https://codimd.apps.education.fr/p/ao-fb52V7#/">`

Rendu :

<embed width="760" height="448" src="https://codimd.apps.education.fr/p/ao-fb52V7#/">

## Solution régionale

Support de formation pour des [RCD avec l'ENT](https://codimd.apps.education.fr/p/rFUk-bQtd#/)



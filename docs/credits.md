---
author: Boris Garcia
title: Crédits
---

Le site est hébergé par [La Forge des Communs Numériques Éducatifs](https://forge.apps.education.fr/).

Le site est construit avec [`mkdocs`](https://www.mkdocs.org/) et en particulier [`mkdocs-material`](https://squidfunk.github.io/mkdocs-material/).

![](https://minio.apps.education.fr/codimd-prod/uploads/upload_0d5e8f92994a9229e137c567d3386e47.png)



